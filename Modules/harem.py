#Totally recreating !harem for you guys
# -*- coding: utf-8 -*-

import sopel.module
import sopel.trigger
import random



haremarray = [
    #These are currently ordered by joining, or for future candidates roughly the order they first met Simon in-game. */
    #in harem
    "Yarra", "Aka", "Qum D'umpe", "Hilstara", "Trin", "Megail", "Altina", "Varia", "Carina", "Nalili", "Balia", "Lynine", "Orilise", "Riala", "Iris", "Janine", "Wynn","Robin", "Dari", "Uyae",
    #not in harem (yet)
    "Sarai", "Esthera", "Tyna", "Vhala", "Ginasta", "Min", "Fuani", "Neranda", "Galvia", "Elleani",
    #Eternal Best Girl
    "Wendis"
    #not currently included: Hester, Andra, Fheliel, Xestris, Sabitha, Palina, Eytria, Dheria, Bertricia, Allue, Biyue, Palina,
    #divine beings, any w/o bust pic, any found only by hacking */
]

emojiarray = [
    ";D", "<3", "^-^", ";3",
    "\\o/",
    "( ° ͜ʖ ͡°)",
    "( ‾ʖ̫‾)",
    "(✿◠‿◠)",
    "(ツ)"
]

specializedresponses = {
    "dmitry":"Dari, Nalili, Qum, Hilstara, Carina",
    "decanter":"Janine, Robin, Wendis, Galvia, Tyna, Riala, Sarai, Yarra",
    "kyasuko":"Galvia, Hilstara, Varia, Balia, Robin, Sarai, Dari, Fuani, Uyae",
    "sandwichchef":"Galvia, Hilstara, Varia, Balia, Robin, Sarai, Dari, Fuani, Uyae",
    "serel":"Robin, Wynn, Vhala, Balia, Orilise, Sarai",
    "serelius":"Robin, Wynn, Vhala, Balia, Orilise, Sarai",
    "okra":"Robin, Nalili",
    "betatester":"Megail, Ginasta, Esthera, Wendis, Riala, Qum D'umpe, Orilise",
    "lorddarkfor":"Sarai, Nalili, Hilstara",
    "ahm":"Yarra, Aka, Hilstara, Robin",
    "firstlord":"Varia, Trin, Yarra, Janine",
    "implevon":"Robin, Janine, Qum D'umpe, Dari, Megail",
    "lostone":"Esthera, Sarai, Riala, Aka, Hilstara",
    "daitya":"Aka, Esthera, Altina, Tyna",
    "han":"Hilstara, Varia, Iris",
    "togo":"Qum, Riala, Varia, Iris, Vhala"
}

def maybeanemoji():
    if random.randint(1,10) <= 4:
        return "".join(str(x) for x in random.sample(emojiarray , 1))    
    else:
        return ""

def isint(value):
    try:
        int(value)
        return True
    except ValueError:
        return False

def inresponses(value):
    try:
        specializedresponses[value]
        return True
    except KeyError:
        return False



@sopel.module.commands("(?i)harem")
def harem(bot, trigger):
    if trigger.group(2) is None:
        bot.say(", ".join(str(x) for x in random.sample(haremarray, random.randint(3,5))) +" "+ maybeanemoji())
    elif isint(trigger.group(3)) == True:
        if int(trigger.group(3)) < 0:
            bot.say("You'll never have a harem with that kind of negativity.") #if someone tries to be a smartarse and input a negative
        elif int(trigger.group(3)) == 0:
            bot.say("A harem of zero... What is that? A hugpillow collection? I don't know what you want.")
        elif int(trigger.group(3)) == 1:
            bot.say("Let's say best girl is... " + ", ".join(str(x) for x in random.sample(haremarray, 1)) +" "+ maybeanemoji())
        elif int(trigger.group(3)) > len(haremarray):
            bot.say("Let's just list all " + str(len(haremarray)+1) + " possibilities " + ", ".join(str(x) for x in haremarray))
        else:
            bot.say(", ".join(str(x) for x in random.sample(haremarray, int(trigger.group(3)))) +" "+ maybeanemoji())
    
    elif str(trigger.group(3)) == "for":
        if trigger.group(4) == "me" and inresponses(trigger.nick.lower()) == True:
            bot.say(str(specializedresponses[trigger.nick.lower()]) +" "+ str(maybeanemoji()))
        elif trigger.group(4) == "me" and inresponses(trigger.nick.lower()) == False:
            bot.say("You are not important enough to have a customized response, get lost.")
        elif inresponses(trigger.group(4).lower()) == False:
            bot.say(trigger.group(4) + " is not important enough to have a customized response.")
        else:
            bot.say(str(specializedresponses[trigger.group(4).lower()]) +" "+ str(maybeanemoji()))
    else:
        bot.say(", ".join(str(x) for x in random.sample(haremarray, random.randint(3,5))) +" "+ maybeanemoji())