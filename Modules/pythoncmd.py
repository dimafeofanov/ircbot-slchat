import sopel.module

@sopel.module.commands("(?i)python")
def python(bot, trigger):
    bot.say("The definitive python workflow: https://is.gd/pythonworkflow")